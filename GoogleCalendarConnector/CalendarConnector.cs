﻿using AppointmentModule.Models;
using CommandLineRunModule;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using GoogleCalendarConnector.Models;
using GoogleCalendarConnector.Services;
using GoogleCalendarConnector.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GoogleCalendarConnector
{


    public class CalendarConnector : AppController
    {
        static string[] Scopes = { CalendarService.Scope.Calendar };
        static string ApplicationName = "OModules Calendar Sync";

        public async Task<ResultItem<DeleteEventResult>> DeleteEvent(DeleteEventRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteEventResult>>(async() =>
           {
               var result = new ResultItem<DeleteEventResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new DeleteEventResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateDeleteEventsRequest(request, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ElasticService(Globals);

               request.MessageOutput?.OutputInfo("Get model...");
               var modelResult = await elasticAgent.GetDeleteEventsModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               var deleteConfig = modelResult.Result.DeleteEventsConfig;
               var syncConfig = modelResult.Result.ConfigToSyncConfig.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).First();
               result.Result.EventsDeleteResult = modelResult.Result.ConfigToGoogleEvents.Select(rel => new EventDeleteResult
               {
                   Event = new clsOntologyItem
                   {
                       GUID = rel.ID_Other,
                       Name = rel.Name_Other,
                       GUID_Parent = rel.ID_Parent_Other,
                       Type = rel.Ontology
                   },
                   Result = Globals.LState_Success.Clone()
               }).ToList();

               var syncModelRequest = new CalendarSyncRequest(syncConfig.GUID) { MessageOutput = request.MessageOutput };
               request.MessageOutput?.OutputInfo("Get Sync Model...");
               var serviceResult = await elasticAgent.GetSyncCalendarModel(syncModelRequest, false);
               request.MessageOutput?.OutputInfo("Have Sync Model");

               result.ResultState = serviceResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var commandLineRunConnector = new CommandLineRunController(Globals);

               request.MessageOutput?.OutputInfo($"Calendar Sync: {syncConfig.Name}");
               var commandLinerun = serviceResult.CalendarSyncToCommandLineRun.Select(rel => new clsOntologyItem { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other, Type = Globals.Type_Object }).FirstOrDefault();
               var commandLinerunResult = await commandLineRunConnector.GetCommandLineRun(commandLinerun);

               result.ResultState = commandLinerunResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Credential file";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }



               if (string.IsNullOrEmpty(commandLinerunResult.Result.CodeParsed))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No valid Credential-File";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
               }
               request.MessageOutput?.OutputInfo($"Found Credential-File");


               var credentialFilePath = Environment.ExpandEnvironmentVariables("%TEMP%\\credentials.json");

               try
               {
                   if (File.Exists(credentialFilePath))
                   {
                       File.Delete(credentialFilePath);
                   }
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Delete Credentialfile: {ex.Message}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               try
               {
                   File.WriteAllText(credentialFilePath, commandLinerunResult.Result.CodeParsed);
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Create Credentialfile: {ex.Message}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo($"Wrote Credential-File");

               UserCredential credential;

               CalendarService service;
               try
               {
                   using (var stream =
                   new FileStream(credentialFilePath, FileMode.Open, FileAccess.Read))
                   {
                       // The file token.json stores the user's access and refresh tokens, and is created
                       // automatically when the authorization flow completes for the first time.
                       string credPath = "token.json";
                       credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                           GoogleClientSecrets.Load(stream).Secrets,
                           Scopes,
                           "user",
                           CancellationToken.None,
                           new FileDataStore(credPath, true)).Result;
                       request.MessageOutput?.OutputInfo($"Credential file saved to: {credPath}");
                   }

                   // Create Google Calendar API service.
                   service = new CalendarService(new BaseClientService.Initializer()
                   {
                       HttpClientInitializer = credential,
                       ApplicationName = ApplicationName,
                   });


                   request.MessageOutput?.OutputInfo($"Iterate {result.Result.EventsDeleteResult.Count} events...");
                   foreach (var eventItem in result.Result.EventsDeleteResult)
                   {
                       
                       var deleteRequest = service.Events.Delete("primary", eventItem.Event.Name);
                       var deleteResult = deleteRequest.Execute();
                       if (!string.IsNullOrEmpty( deleteResult))
                       {
                           eventItem.Result = Globals.LState_Error.Clone();
                           eventItem.Result.Additional1 = deleteResult;
                           continue;
                       }
                   }

                   request.MessageOutput?.OutputInfo($"Delete {result.Result.EventsDeleteResult.Where(evt => evt.Result.GUID == Globals.LState_Success.GUID).Count()} events.");
                   result.ResultState = await elasticAgent.DeleteEvents(result.Result);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   }
                   return result;
                   
               }
               catch (Exception ex)
               {

                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while getting Google Events: {ex.Message}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
           });

            return taskResult;
        }

        public async Task<CalendarSyncResult> SyncCalendar(CalendarSyncRequest request)
        {
            var taskResult = await Task.Run<CalendarSyncResult>(async() =>
            {
                request.MessageOutput?.OutputInfo("Start Sync");
                var result = new CalendarSyncResult
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var elasticService = new ElasticService(Globals);

                request.MessageOutput?.OutputInfo("Get Model...");
                var serviceResult = await elasticService.GetSyncCalendarModel(request);
                request.MessageOutput?.OutputInfo("Have Model");

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var syncAppointments = serviceResult.SyncAttributes.FirstOrDefault(att => att.ID_AttributeType == Config.LocalData.AttributeType_SyncAppointments.GUID )?.Val_Bit.Value ?? false;
                var syncPersonalDates = serviceResult.SyncAttributes.FirstOrDefault(att => att.ID_AttributeType == Config.LocalData.AttributeType_SyncPersonalDates.GUID )?.Val_Bit.Value ?? false;

                request.MessageOutput?.OutputInfo($"Sync Appointments: {syncAppointments}, Sync Personal Dates: {syncPersonalDates}");

                var commandLineRunConnector = new CommandLineRunController(Globals);


                request.MessageOutput?.OutputInfo($"Found {serviceResult.CalendarSyncs.Count} Calendar Sync Definitions.");
                foreach (var calendarSync in serviceResult.CalendarSyncs)
                {
                    request.MessageOutput?.OutputInfo($"Calendar Sync: {calendarSync.Name}");
                    var commandLinerun = serviceResult.CalendarSyncToCommandLineRun.Select(rel => new clsOntologyItem { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other, Type = Globals.Type_Object }).FirstOrDefault();
                    var commandLinerunResult = await commandLineRunConnector.GetCommandLineRun(commandLinerun);

                    result.ResultState = commandLinerunResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Credential file";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    

                    if (string.IsNullOrEmpty(commandLinerunResult.Result.CodeParsed) )
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No valid Credential-File";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    }
                    request.MessageOutput?.OutputInfo($"Found Credential-File");


                    var credentialFilePath = Environment.ExpandEnvironmentVariables("%TEMP%\\credentials.json");

                    try
                    {
                        if (File.Exists(credentialFilePath))
                        {
                            File.Delete(credentialFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Delete Credentialfile: {ex.Message}";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    try
                    {
                        File.WriteAllText(credentialFilePath, commandLinerunResult.Result.CodeParsed);
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Create Credentialfile: {ex.Message}";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Wrote Credential-File");

                    var googleEventsOnto = from user in serviceResult.CalendarSyncToUser.Where(rel => rel.ID_Object == calendarSync.GUID)
                                           join appointment in serviceResult.UserToAppointments on user.ID_Other equals appointment.ID_Other
                                           join googlevent in serviceResult.GoogleEventsToAppointments on appointment.ID_Object equals googlevent.ID_Other
                                           select new { appointment, googlevent};
                    result.EventSync.OntoAppointmentEvents = (from userAppointment in serviceResult.UserToAppointments
                                     join start in serviceResult.AppointmentAttributes.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Start.GUID) on userAppointment.ID_Object equals start.ID_Object
                                     join end in serviceResult.AppointmentAttributes.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Ende.GUID) on userAppointment.ID_Object equals end.ID_Object
                                     join googleEvent in googleEventsOnto on userAppointment.ID_Object equals googleEvent.appointment.ID_Object into googleEvents1
                                     from googleEvent in googleEvents1.DefaultIfEmpty()
                                        select new OntoAppointmentEvent
                                        {
                                            Appointment = new Appointment
                                            {
                                                IdAppointment = userAppointment.ID_Object,
                                                NameAppointment = userAppointment.Name_Object,
                                                IdAttributeStart = start.ID_Attribute,
                                                Start = start.Val_Datetime.Value,
                                                IdAttributeEnd = end.ID_Attribute,
                                                End = end.Val_Datetime

                                            },
                                            IdGoogleEvent = googleEvent != null ? googleEvent.googlevent.ID_Object : null,
                                            NameGoogleEvent = googleEvent != null ? googleEvent.googlevent.Name_Object : null
                                        }).ToList();

                    var googleEvents = from googleEvent in serviceResult.GoogleEventToPartners
                                       join googleEventAttribute in serviceResult.GoogleEventPersonalDateAttributes on googleEvent.ID_Object equals googleEventAttribute.ID_Object
                                       select new { googleEvent, googleEventAttribute };


                   result.EventSync.OntoPersonalDateEvents = (from personalDate in serviceResult.PersonalDates
                                                              join partner in serviceResult.NatuerlichePersonToPartner on personalDate.ID_Object equals partner.ID_Object
                                                              join googleEvent in googleEvents on new { personalDate.ID_Attribute, partner.ID_Other } equals new { ID_Attribute = googleEvent.googleEventAttribute.Val_String, googleEvent.googleEvent.ID_Other } into googleEvents1
                                                              from googleEvent in googleEvents1.DefaultIfEmpty()
                                                              select new OntoPersonalDateEvent
                                                              {
                                                                  IdGoogleEvent = googleEvent?.googleEvent.ID_Object,
                                                                  NameGoogleEvent = googleEvent?.googleEvent.Name_Object,
                                                                  Partner = new clsOntologyItem
                                                                  {
                                                                      GUID = partner.ID_Other,
                                                                      Name = partner.Name_Other,
                                                                      GUID_Parent = partner.ID_Parent_Other,
                                                                      Type = partner.Ontology
                                                                  },
                                                                  PersonalDate = personalDate
                                                              }).ToList();



                    UserCredential credential;

                    CalendarService service;
                    try
                    {
                        using (var stream =
                        new FileStream(credentialFilePath, FileMode.Open, FileAccess.Read))
                        {
                            // The file token.json stores the user's access and refresh tokens, and is created
                            // automatically when the authorization flow completes for the first time.
                            string credPath = "token.json";
                            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                                GoogleClientSecrets.Load(stream).Secrets,
                                Scopes,
                                "user",
                                CancellationToken.None,
                                new FileDataStore(credPath, true)).Result;
                            request.MessageOutput?.OutputInfo($"Credential file saved to: {credPath}");
                        }

                        // Create Google Calendar API service.
                        service = new CalendarService(new BaseClientService.Initializer()
                        {
                            HttpClientInitializer = credential,
                            ApplicationName = ApplicationName,
                        });

                        // Define parameters of request.
                        EventsResource.ListRequest googleRequest = service.Events.List("primary");
                        googleRequest.ShowDeleted = false;
                        googleRequest.SingleEvents = true;
                        googleRequest.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                        request.MessageOutput?.OutputInfo($"Get Events.");
                        // List events.
                        Events events = googleRequest.Execute();

                        result.EventSync.GoogleEvents = events.Items;
                        request.MessageOutput?.OutputInfo($"Found {events.Items} Events.");

                        result.EventSync.OntoAppointmentEvents.ForEach(eventItm =>
                        {
                            eventItm.GoogleEvent = result.EventSync.GoogleEvents.FirstOrDefault(gEvent => gEvent.Id == eventItm.NameGoogleEvent);
                        });

                        result.EventSync.OntoPersonalDateEvents.ForEach(eventItm =>
                        {
                            eventItm.GoogleEvent = result.EventSync.GoogleEvents.FirstOrDefault(gEvent => gEvent.Id == eventItm.NameGoogleEvent);
                        });
                    }
                    catch (Exception ex)
                    {

                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Error while getting Google Events: {ex.Message}";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (request.DeleteGoogleEvents)
                    {
                        request.MessageOutput?.OutputInfo($"Delete {result.EventSync.GoogleEvents.Count} Events...");
                        foreach (var item in result.EventSync.GoogleEvents)
                        {
                            String calendarId = "primary";
                            EventsResource.DeleteRequest deleteRequest = service.Events.Delete(calendarId, item.Id);
                            var deleteResult = deleteRequest.Execute();
                        }
                        request.MessageOutput?.OutputInfo($"Deleted {result.EventSync.GoogleEvents.Count} Events.");
                    }
                    else
                    {
                        if (syncAppointments)
                        {
                            request.MessageOutput?.OutputInfo($"Sync Appointmtents...");
                            result.ResultState = await result.EventSync.FillSyncAppointmentItems(Globals, service);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = result.ResultState.Additional1;
                            }
                            else
                            {
                                request.MessageOutput?.OutputInfo($"Synced Appointmtents");
                            }
                        }

                        if (syncPersonalDates)
                        {
                            request.MessageOutput?.OutputInfo($"Sync Personal Dates...");
                            result.ResultState = await result.EventSync.FillSyncPersonalDateItems(Globals, service);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = result.ResultState.Additional1;
                            }
                            else
                            {
                                request.MessageOutput?.OutputInfo($"Synced Personal Dates");
                            }
                        }
                    }
                    
                    
                }

                return result;
            });

            return taskResult;
        }



        public CalendarConnector(Globals globals) :  base(globals)
        {
        }
    }
}

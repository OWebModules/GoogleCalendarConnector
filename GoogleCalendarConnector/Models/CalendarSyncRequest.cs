﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class CalendarSyncRequest
    {
        public string IdCalendarSync { get; set; }
        public bool DeleteGoogleEvents { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
        
        public CalendarSyncRequest(string idGoogleCalendarSync)
        {
            IdCalendarSync = idGoogleCalendarSync;
        }
    }
}

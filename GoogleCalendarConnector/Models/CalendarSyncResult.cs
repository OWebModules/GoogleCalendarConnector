﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class CalendarSyncResult
    {
        public clsOntologyItem ResultState { get; set; }
        public EventSync EventSync { get; set; } = new EventSync();
    }
}

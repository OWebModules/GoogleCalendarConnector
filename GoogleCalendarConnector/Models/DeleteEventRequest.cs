﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class DeleteEventRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public DeleteEventRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}

﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class DeleteEventResult
    {
        public List<EventDeleteResult> EventsDeleteResult { get; set; } = new List<EventDeleteResult>();
    }

    public class EventDeleteResult
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem Event { get; set; }
    }
}

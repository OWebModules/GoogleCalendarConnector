﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class DeleteEventsModel
    {
        public clsOntologyItem DeleteEventsConfig { get; set; }
        public List<clsObjectRel> ConfigToGoogleEvents { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigToSyncConfig { get; set; } = new List<clsObjectRel>();
    }
}

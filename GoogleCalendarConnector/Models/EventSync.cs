﻿using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace GoogleCalendarConnector.Models
{
    public class EventSync
    {
        public IList<Event> GoogleEvents { get; set; } = new List<Event>();
        public List<OntoAppointmentEvent> OntoAppointmentEvents { get; set; } = new List<OntoAppointmentEvent>();
        public List<OntoPersonalDateEvent> OntoPersonalDateEvents { get; set; } = new List<OntoPersonalDateEvent>();
        public List<Event> ToOntoDbInsert { get; private set; } = new List<Event>();
        public List<OntoAppointmentEvent> InsertedAppointmentEventsToGoogleCalendar { get; private set; } = new List<OntoAppointmentEvent>();
        public List<OntoPersonalDateEvent> InsertedPersonalEventsToGoogleCalendar { get; private set; } = new List<OntoPersonalDateEvent>();
        public List<Event> ToOntoDbUpdate { get; private set; } = new List<Event>();
        public List<Event> ToGoogleCalendarUpdate { get; private set; } = new List<Event>();

        public async Task<clsOntologyItem> FillSyncAppointmentItems(Globals globals, CalendarService service)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);

                

                foreach (var ontoEvent in OntoAppointmentEvents)
                {
                    
                    if (ontoEvent.GoogleEvent == null)
                    {
                        ontoEvent.GoogleEvent = new Event()
                        {
                            Summary = ontoEvent.Appointment.NameAppointment,
                            Location = "",
                            Description = "",
                            Start = new EventDateTime()
                            {
                                DateTime = ontoEvent.Appointment.Start,
                                TimeZone = "Europe/Berlin",
                            },
                            End = new EventDateTime()
                            {
                                DateTime = ontoEvent.Appointment.End,
                                TimeZone = "Europe/Berlin",
                            }
                        };
                    }
                    

                    try
                    {
                        if (string.IsNullOrEmpty(ontoEvent.IdGoogleEvent))
                        {
                            String calendarId = "primary";
                            EventsResource.InsertRequest request = service.Events.Insert(ontoEvent.GoogleEvent, calendarId);
                            Event createdEvent = request.Execute();
                            Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);

                            var eventToSave = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = createdEvent.Id,
                                GUID_Parent = Config.LocalData.Class_Google_Event.GUID,
                                Type = globals.Type_Object
                            };

                            transaction.ClearItems();
                            result = transaction.do_Transaction(eventToSave);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Google Event";
                                return result;
                            }
                            var rel = relationConfig.Rel_ObjectRelation(eventToSave, new clsOntologyItem
                            {
                                GUID = ontoEvent.Appointment.IdAppointment,
                                Name = ontoEvent.Appointment.NameAppointment,
                                GUID_Parent = Config.LocalData.Class_Appointment.GUID,
                                Type = globals.Type_Object
                            }, new clsOntologyItem { GUID = Config.LocalData.ClassRel_Google_Event_is_Appointment.ID_RelationType });

                            result = transaction.do_Transaction(rel);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Relation between Google Event and Appointment";
                                transaction.rollback();
                                return result;
                            }

                            ontoEvent.IdGoogleEvent = eventToSave.GUID;
                            ontoEvent.NameGoogleEvent = eventToSave.Name;

                            InsertedAppointmentEventsToGoogleCalendar.Add(ontoEvent);
                        }
                        else
                        {

                            var start = new EventDateTime()
                            {
                                DateTime = ontoEvent.Appointment.Start,
                                TimeZone = "Europe/Berlin",
                            };

                            var end = new EventDateTime()
                            {
                                DateTime = ontoEvent.Appointment.End,
                                TimeZone = "Europe/Berlin",
                            };

                            if (ontoEvent.GoogleEvent.Summary != ontoEvent.Appointment.NameAppointment ||
                                ontoEvent.GoogleEvent.Start.DateTime != start.DateTime ||
                                ontoEvent.GoogleEvent.End.DateTime != end.DateTime)
                            {

                                ontoEvent.GoogleEvent.Summary = ontoEvent.Appointment.NameAppointment;
                                ontoEvent.GoogleEvent.Start = new EventDateTime()
                                {
                                    DateTime = ontoEvent.Appointment.Start,
                                    TimeZone = "Europe/Berlin",
                                };
                                ontoEvent.GoogleEvent.End = new EventDateTime()
                                {
                                    DateTime = ontoEvent.Appointment.End,
                                    TimeZone = "Europe/Berlin",
                                };
                                String calendarId = "primary";
                                EventsResource.UpdateRequest request = service.Events.Update(ontoEvent.GoogleEvent, calendarId, ontoEvent.NameGoogleEvent);
                                Event createdEvent = request.Execute();
                            }
                            
                        }
                        

                    }
                    catch (Exception ex)
                    {

                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;
                    }
                    
                }

                return result;
            });

            return taskResult;
            
        }

        public async Task<clsOntologyItem> FillSyncPersonalDateItems(Globals globals, CalendarService service)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);



                foreach (var ontoEvent in OntoPersonalDateEvents)
                {
                    var name = ontoEvent.PersonalDate.ID_AttributeType == Config.LocalData.AttributeType_Geburtsdatum.GUID ? "Geburtstag" : "Todestag";
                    name = $"{name}: {ontoEvent.Partner.Name}";

                    var start = ontoEvent.PersonalDate.Val_Date.Value.Date;
                    var end = ontoEvent.PersonalDate.Val_Date.Value.Date.AddDays(1).AddSeconds(-1);

                    if (ontoEvent.GoogleEvent == null)
                    {
                        ontoEvent.GoogleEvent = new Event()
                        {
                            Summary = name,
                            Location = "",
                            Description = "",
                            Start = new EventDateTime()
                            {
                                Date = start.Date.ToString("yyyy-MM-dd"),
                                TimeZone = "Europe/Berlin"
                            },
                            End = new EventDateTime()
                            {
                                Date = end.Date.ToString("yyyy-MM-dd"),
                                TimeZone = "Europe/Berlin",
                            },
                            Recurrence = new List<string> { $"RRULE:FREQ=YEARLY;UNTIL={DateTime.Now.AddYears(100).Year}1231T235959Z" },
                        };

                    }
                    

                    try
                    {
                        String calendarId = "primary";
                        if (string.IsNullOrEmpty(ontoEvent.IdGoogleEvent))
                        {
                            EventsResource.InsertRequest request = service.Events.Insert(ontoEvent.GoogleEvent, calendarId);
                            Event createdEvent = request.Execute();
                            Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);

                            var eventToSave = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = createdEvent.Id,
                                GUID_Parent = Config.LocalData.Class_Google_Event.GUID,
                                Type = globals.Type_Object
                            };

                            transaction.ClearItems();
                            result = transaction.do_Transaction(eventToSave);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Google Event";
                                return result;
                            }
                            var rel = relationConfig.Rel_ObjectRelation(eventToSave, new clsOntologyItem
                            {
                                GUID = ontoEvent.Partner.GUID,
                                Name = ontoEvent.Partner.Name,
                                GUID_Parent = Config.LocalData.Class_Partner.GUID,
                                Type = globals.Type_Object
                            }, new clsOntologyItem { GUID = Config.LocalData.ClassRel_Google_Event_belongs_to_Partner.ID_RelationType });

                            result = transaction.do_Transaction(rel);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Relation between Google Event and Appointment";
                                transaction.rollback();
                                return result;
                            }

                            var relAttrib = relationConfig.Rel_ObjectAttribute(eventToSave,
                                ontoEvent.PersonalDate.ID_AttributeType == Config.LocalData.AttributeType_Geburtsdatum.GUID ?
                                Config.LocalData.AttributeType_IsGeburtsdatum : Config.LocalData.AttributeType_IsTodesdatum, true);

                            result = transaction.do_Transaction(relAttrib);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Relation between Google Event and Personal Date type";
                                transaction.rollback();
                                return result;
                            }

                            var relIdAttrib = relationConfig.Rel_ObjectAttribute(eventToSave,
                                Config.LocalData.AttributeType_IdAttribute,
                                ontoEvent.PersonalDate.ID_Attribute);

                            result = transaction.do_Transaction(relIdAttrib);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                result.Additional1 = "Error while saving Relation between Google Event and IdAttribute of Personal Date";
                                transaction.rollback();
                                return result;
                            }

                            ontoEvent.IdGoogleEvent = eventToSave.GUID;
                            ontoEvent.NameGoogleEvent = eventToSave.Name;

                            InsertedPersonalEventsToGoogleCalendar.Add(ontoEvent);
                        }
                        else
                        {
                            if (ontoEvent.GoogleEvent.Summary != name ||
                                   ontoEvent.GoogleEvent.Start != new EventDateTime()
                                   {
                                       Date = start.Date.ToString("yyyy-MM-dd"),
                                       TimeZone = "Europe/Berlin",
                                   } ||
                                   ontoEvent.GoogleEvent.End != new EventDateTime()
                                   {
                                       Date = end.Date.ToString("yyyy-MM-dd"),
                                       TimeZone = "Europe/Berlin",
                                   })
                            {

                                ontoEvent.GoogleEvent.Summary = name;
                                ontoEvent.GoogleEvent.Start = new EventDateTime()
                                {
                                    Date = start.Date.ToString("yyyy-MM-dd"),
                                    TimeZone = "Europe/Berlin",
                                };
                                ontoEvent.GoogleEvent.End = new EventDateTime()
                                {
                                    Date = end.Date.ToString("yyyy-MM-dd"),
                                    TimeZone = "Europe/Berlin",
                                };
                                EventsResource.UpdateRequest request = service.Events.Update(ontoEvent.GoogleEvent, calendarId, ontoEvent.NameGoogleEvent);
                                Event createdEvent = request.Execute();
                            }
                        }
                        
                        

                    }
                    catch (Exception ex)
                    {

                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;
                    }

                }

                return result;
            });

            return taskResult;

        }
    }
}

﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class ModelResult
    {
        public clsOntologyItem ResultState { get; set; }
        public List<clsObjectAtt> SyncAttributes { get; set; } = new List<clsObjectAtt>();

        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> CalendarSyncs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> CalendarSyncToCommandLineRun { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CalendarSyncToUser { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> UserToAppointments { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> AppointmentAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> GoogleEventsToAppointments { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> PersonalDates { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> NatuerlichePersonToPartner { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> GoogleEventToPartners { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> GoogleEventPersonalDateAttributes { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> PartnersToSync { get; set; } = new List<clsObjectRel>();
    }
}

﻿using Google.Apis.Calendar.v3.Data;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Models
{
    public class OntoPersonalDateEvent
    {
        public clsOntologyItem Partner { get; set; }
        public clsObjectAtt PersonalDate { get; set; }
        public string IdGoogleEvent { get; set; }
        public string NameGoogleEvent { get; set; }
        public Event GoogleEvent { get; set; }
    }
}

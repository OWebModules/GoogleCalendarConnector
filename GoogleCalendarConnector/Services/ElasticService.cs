﻿using GoogleCalendarConnector.Models;
using GoogleCalendarConnector.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Services
{
    public class ElasticService : ElasticBaseAgent
    {
        public async Task<clsOntologyItem> DeleteEvents(DeleteEventResult deleteEventResults)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
           {
               var result = globals.LState_Success.Clone();

               var deleteBase = deleteEventResults.EventsDeleteResult.Where(evt => evt.Result.GUID == globals.LState_Success.GUID);
               var deleteRelations = deleteBase.Select(evt => new clsObjectRel
               {
                   ID_Object = evt.Event.GUID
               }).ToList();

               deleteRelations.AddRange(deleteBase.Select(evt => new clsObjectRel
               {
                   ID_Other = evt.Event.GUID
               }));

               var deleteItems = deleteBase.Select(evt => evt.Event).ToList();

               var dbWriter = new OntologyModDBConnector(globals);

               if (deleteRelations.Any())
               {
                   result = dbWriter.DelObjectRels(deleteRelations);
                   if (result.GUID == globals.LState_Error.GUID)
                   {
                       result.Additional1 = "Error while deleting relations!";
                       return result;
                   }
               }

               if (deleteItems.Any())
               {
                   result = dbWriter.DelObjects(deleteItems);

                   if (result.GUID == globals.LState_Error.GUID)
                   {
                       result.Additional1 = "Error while deleting events!";
                       return result;
                   }
               }
               
               return result;
           });

            return taskResult;
        }
        public async Task<ResultItem<DeleteEventsModel>> GetDeleteEventsModel(DeleteEventRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteEventsModel>>(() =>
            {
                var result = new ResultItem<DeleteEventsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new DeleteEventsModel()
                };

                #region request-validation

                result.ResultState = ValidationController.ValidateDeleteEventsRequest(request, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                #endregion

                #region config
                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = Config.LocalData.Class_Delete_Google_Event.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Delete-Config!";
                    return result;
                }

                result.Result.DeleteEventsConfig = dbReaderConfig.Objects1.First();

                result.ResultState = ValidationController.ValidateGetDeleteEvents(result.Result, globals, nameof(result.Result.DeleteEventsConfig));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                #endregion

                #region sync-config
                var searchGoogleSyncConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.DeleteEventsConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Delete_Google_Event_belongs_to_Google_Calcendarsync.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Delete_Google_Event_belongs_to_Google_Calcendarsync.ID_Class_Right
                    }
                };

                var dbReaderGoogleSyncConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGoogleSyncConfig.GetDataObjectRel(searchGoogleSyncConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relation between the delete config and the sync config!";
                    return result;
                }

                result.Result.ConfigToSyncConfig = dbReaderGoogleSyncConfig.ObjectRels;

                result.ResultState = ValidationController.ValidateGetDeleteEvents(result.Result, globals, nameof(result.Result.ConfigToSyncConfig));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                #endregion

                #region events to delete

                var searchEvents = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.DeleteEventsConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Delete_Google_Event_contains_Google_Event.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Delete_Google_Event_contains_Google_Event.ID_Class_Right
                    }
                };

                var dbReaderEvents = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderEvents.GetDataObjectRel(searchEvents);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the events to delete!";
                    return result;
                }

                result.Result.ConfigToGoogleEvents = dbReaderEvents.ObjectRels;

                result.ResultState = ValidationController.ValidateGetDeleteEvents(result.Result, globals, nameof(result.Result.ConfigToGoogleEvents));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                #endregion

                return result;
            });

            return taskResult;
        }

        public async Task<ModelResult> GetSyncCalendarModel(CalendarSyncRequest request, bool getAppointmentsAndPersonalDates = true)
        {
            var taskResult = await Task.Run<ModelResult>(() =>
            {
                var result = new ModelResult
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdCalendarSync,
                        GUID_Parent = Config.LocalData.Class_Google_Calcendarsync.GUID
                    }
                };

                var dbReaderRootConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the root-config!";
                    return result;
                }

                result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

                if (result.RootConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No root-config found!";
                    return result;
                }

                var searchSubconfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.RootConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Google_Calcendarsync_contains_Google_Calcendarsync.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Google_Calcendarsync_contains_Google_Calcendarsync.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubconfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the sub-configs!";
                    return result;
                }

                result.CalendarSyncs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.CalendarSyncs.Any())
                {
                    result.CalendarSyncs.Add(result.RootConfig);
                }


                var searchSyncAtts = result.CalendarSyncs.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_SyncAppointments.GUID
                }).ToList();

                searchSyncAtts.AddRange(result.CalendarSyncs.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_SyncPersonalDates.GUID

                }));

                var dbReaderSyncAtts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSyncAtts.GetDataObjectAtt(searchSyncAtts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Sync Attributes";
                    return result;
                }

                if (!dbReaderSyncAtts.ObjAtts.Any(att => att.Val_Bit.Value))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You have to set a flag to sync Appointments or Personal Dates";
                    return result;
                }

                result.SyncAttributes = dbReaderSyncAtts.ObjAtts;

                var searchSubSyncs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdCalendarSync,
                        ID_RelationType = Config.LocalData.ClassRel_Google_Calcendarsync_contains_Google_Calcendarsync.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Google_Calcendarsync_contains_Google_Calcendarsync.ID_Class_Right
                    }
                };


                var searchSyncsToCommandLineRuns = result.CalendarSyncs.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Google_Calcendarsync_Credential_File_Comand_Line__Run_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Google_Calcendarsync_Credential_File_Comand_Line__Run_.ID_Class_Right
                }).ToList();

                var dbReaderSyncsToCommandLineRuns = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSyncsToCommandLineRuns.GetDataObjectRel(searchSyncsToCommandLineRuns);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Credential File";
                    return result;
                }

                result.CalendarSyncToCommandLineRun = dbReaderSyncsToCommandLineRuns.ObjectRels;

                if (!result.CalendarSyncToCommandLineRun.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Credentialfiles found!";
                    return result;
                }

                var searchCalendarSyncToUser = result.CalendarSyncs.Select(sync => new clsObjectRel
                {
                    ID_Object = sync.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Google_Calcendarsync_belongs_to_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Google_Calcendarsync_belongs_to_user.ID_Class_Right
                }).ToList();

                var dbReaderCalendarSyncToUser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCalendarSyncToUser.GetDataObjectRel(searchCalendarSyncToUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading User";
                    return result;
                }

                result.CalendarSyncToUser = dbReaderCalendarSyncToUser.ObjectRels;

                if (!result.CalendarSyncToUser.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No User found!";
                    return result;
                }

                if (getAppointmentsAndPersonalDates)
                {
                    var searchUserAppointments = result.CalendarSyncToUser.Select(rel => new clsObjectRel
                    {
                        ID_Other = rel.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Appointment_belongs_to_user.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Appointment_belongs_to_user.ID_Class_Left
                    }).ToList();

                    var dbReaderAppointmentToUser = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderAppointmentToUser.GetDataObjectRel(searchUserAppointments);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Userappointments";
                        return result;
                    }

                    result.UserToAppointments = dbReaderAppointmentToUser.ObjectRels;

                    var searchAppointmentAttribute = result.UserToAppointments.Select(rel => new clsObjectAtt
                    {
                        ID_Object = rel.ID_Object,
                        ID_AttributeType = Config.LocalData.AttributeType_Start.GUID
                    }).ToList();

                    searchAppointmentAttribute.AddRange(result.UserToAppointments.Select(rel => new clsObjectAtt
                    {
                        ID_Object = rel.ID_Object,
                        ID_AttributeType = Config.LocalData.AttributeType_Ende.GUID
                    }));

                    var dbReaderAppointmentAttributes = new OntologyModDBConnector(globals);

                    if (searchAppointmentAttribute.Any())
                    {
                        result.ResultState = dbReaderAppointmentAttributes.GetDataObjectAtt(searchAppointmentAttribute);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while loading Appointmentattributes";
                            return result;
                        }
                    }

                    result.AppointmentAttributes = dbReaderAppointmentAttributes.ObjAtts;

                    var searchGoogleEventToAppointment = result.UserToAppointments.Select(rel => new clsObjectRel
                    {
                        ID_Other = rel.ID_Object,
                        ID_RelationType = Config.LocalData.ClassRel_Google_Event_is_Appointment.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Google_Event_is_Appointment.ID_Class_Left
                    }).ToList();


                    var dbReaderGoogleEventToAppointment = new OntologyModDBConnector(globals);

                    if (searchGoogleEventToAppointment.Any())
                    {
                        result.ResultState = dbReaderGoogleEventToAppointment.GetDataObjectRel(searchGoogleEventToAppointment);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while loading Google-Events";
                            return result;
                        }
                    }


                    result.GoogleEventsToAppointments = dbReaderGoogleEventToAppointment.ObjectRels;

                    var searchPersonalDates = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.AttributeType_Todesdatum.GUID
                        },
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.AttributeType_Geburtsdatum.GUID
                        }
                    };

                    var dbReaderPersonalDates = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPersonalDates.GetDataObjectAtt(searchPersonalDates);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Personal Dates";
                        return result;
                    }

                    result.PersonalDates = dbReaderPersonalDates.ObjAtts;

                    var searchNatuerlichePersonToPartner = dbReaderPersonalDates.ObjAtts.Select(rel => new clsObjectRel
                    {
                        ID_Object = rel.ID_Object,
                        ID_RelationType = Config.LocalData.ClassRel_nat_rliche_Person_belongs_to_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_nat_rliche_Person_belongs_to_Partner.ID_Class_Right
                    }).ToList();

                    if (searchNatuerlichePersonToPartner.Any())
                    {
                        var dbReaderNatuerlichePersonToPartner = new OntologyModDBConnector(globals);

                        result.ResultState = dbReaderNatuerlichePersonToPartner.GetDataObjectRel(searchNatuerlichePersonToPartner);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while loading relation between natürliche person and partner";
                            return result;
                        }
                        result.NatuerlichePersonToPartner = dbReaderNatuerlichePersonToPartner.ObjectRels;
                    }


                    var searchPartnersToSync = result.CalendarSyncs.Select(calSync => new clsObjectRel
                    {
                        ID_Object = calSync.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Google_Calcendarsync_Sync_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Google_Calcendarsync_Sync_Partner.ID_Class_Right
                    }).ToList();

                    var dbReaderPartnersToSync = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPartnersToSync.GetDataObjectRel(searchPartnersToSync);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Partners to sync!";
                        return result;
                    }

                    result.PartnersToSync = dbReaderPartnersToSync.ObjectRels;

                    if (result.PartnersToSync.Any())
                    {
                        result.NatuerlichePersonToPartner = (from natToPart in result.NatuerlichePersonToPartner
                                                             join partner in result.PartnersToSync on natToPart.ID_Other equals partner.ID_Other
                                                             select natToPart).ToList();
                    }

                    var searchGoogleEventsToPartners = result.NatuerlichePersonToPartner.Select(rel => new clsObjectRel
                    {
                        ID_Other = rel.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Google_Event_belongs_to_Partner.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Google_Event_belongs_to_Partner.ID_Class_Left
                    }).ToList();

                    if (searchGoogleEventsToPartners.Any())
                    {
                        var dbReaderGoolgeEventsToPartners = new OntologyModDBConnector(globals);

                        result.ResultState = dbReaderGoolgeEventsToPartners.GetDataObjectRel(searchGoogleEventsToPartners);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while loading relation between google events and partners";
                            return result;
                        }

                        result.GoogleEventToPartners = dbReaderGoolgeEventsToPartners.ObjectRels;
                    }

                    var searchEventAttriutes = result.GoogleEventToPartners.Select(rel => new clsObjectAtt
                    {
                        ID_Object = rel.ID_Object,
                        ID_AttributeType = Config.LocalData.AttributeType_IdAttribute.GUID
                    }).ToList();

                    if (searchEventAttriutes.Any())
                    {
                        var dbReaderEventAttributes = new OntologyModDBConnector(globals);

                        result.ResultState = dbReaderEventAttributes.GetDataObjectAtt(searchEventAttriutes);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while loading Personal Date Event Attributes";
                            return result;
                        }

                        result.GoogleEventPersonalDateAttributes = dbReaderEventAttributes.ObjAtts;
                    }
                }


                return result;
            });

            return taskResult;
        }


        public ElasticService(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}

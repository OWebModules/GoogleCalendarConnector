﻿using GoogleCalendarConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarConnector.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateDeleteEventsRequest(DeleteEventRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetDeleteEvents(DeleteEventsModel model, Globals globals, string propertyName  = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.DeleteEventsConfig))
            {
                if (model.DeleteEventsConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No config found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigToGoogleEvents))
            {
                if (!model.ConfigToGoogleEvents.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Google-Events found!";
                    return result;
                }
                
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigToSyncConfig))
            {
                if (model.ConfigToSyncConfig.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Exact one relation between delete-config and sync-config is allowed! {model.ConfigToSyncConfig.Count} items are in list.";
                    return result;
                }
            }

            return result;
        }
    }
}

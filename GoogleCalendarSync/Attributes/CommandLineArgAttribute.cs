﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarSync.Attributes
{
    public class CommandLineArgAttribute : Attribute
    {
        public string RegexRecognize { get; set; }
        public string KeyValSeperator { get; set; }
        public string Usage { get; set; }
        public string Error { get; set; }
        public bool Obligatory { get; set; }
        public int OrderId { get; set; }

    }
}

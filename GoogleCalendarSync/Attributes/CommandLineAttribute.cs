﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarSync.Attributes
{
    public class CommandLineAttribute : Attribute
    {
        public string Usage { get; set; }
    }
}

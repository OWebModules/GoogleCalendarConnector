﻿using GoogleCalendarSync.Attributes;
using GoogleCalendarSync.Primitives;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleCalendarSync.Models
{
    [CommandLine(Usage = "Sync Ontology Modules with Google Calendar")]
    public class CommandLine
    {
        private string[] args { get; set; }

        [CommandLineArg(RegexRecognize ="(?i)(IdGoogleSync:.*)", 
                        Usage = "[IdGoogleSync:[Id]]", 
                        Error = "Provide a Id for Google Sync", 
                        KeyValSeperator = ":",
                        Obligatory = true,
                        OrderId = 1)]
        public string IdGoogleSync { get; set; }

        [CommandLineArg(RegexRecognize = "(?i)(DeleteEvents)",
                        Usage = "[DeleteEvents]",
                        OrderId = 2)]
        public bool DeleteEvents { get; set; }

        public bool IsValid { get; private set; } = true;
        public string ErrorMessage { get; set; }

        private string toolUsage;
        private StringBuilder sbUsages = new StringBuilder();
        public string Usage
        {
            get
            {
                var usage = "";
                if (!string.IsNullOrEmpty(toolUsage))
                {
                    usage += toolUsage + "\n\n";
                }
                usage += $"{System.AppDomain.CurrentDomain.FriendlyName}: {sbUsages.ToString()}";
                return usage;
            }
        }

        public override string ToString()
        {
            var sbResult = new StringBuilder();
            if (!IsValid)
            {
                sbResult.AppendLine(ErrorMessage);
                sbResult.AppendLine();
            }

            sbResult.AppendLine(Usage);

            return sbResult.ToString();
        }


        public CommandLine(string[] args, Globals globals)
        {
            this.args = args;

            if (args.Length < 1 || args.Length > 3)
            {
                IsValid = false;
                ErrorMessage = "The number of arguments is not valid";
                return;
            }

            var proptAttrValues = this.GetType().GetProperties().Select(prop =>
            {
                var attr = (CommandLineArgAttribute)prop.GetCustomAttributes(false).FirstOrDefault(attr1 => attr1.GetType() == typeof(CommandLineArgAttribute));

                if (attr == null)
                {
                    return null;
                }

                var regex = new Regex(attr.RegexRecognize);

                string value = null;
                foreach (var arg in args)
                {
                    if (regex.Match(arg).Success)
                    {
                        if (attr.KeyValSeperator != null)
                        {
                            var splitted = arg.Split(attr.KeyValSeperator.ToCharArray());

                            value = splitted[1];
                            break;
                        }
                        else
                        {
                            value = arg;
                            break;
                        }

                    }
                }

                var result = new { prop, attr, value };

                return result;
            }).Where(propAttVal => propAttVal != null);

            if (proptAttrValues.Any(propAttValue => propAttValue.attr.Obligatory && propAttValue.value == null))
            {
                IsValid = false;
                return;
            }

            try
            {
                var commandLineAttr = (CommandLineAttribute) this.GetType().GetCustomAttributes(false).FirstOrDefault(attr => attr.GetType() == typeof(CommandLineAttribute));

                if (commandLineAttr != null)
                {
                    toolUsage = commandLineAttr.Usage;
                }

                
                foreach (var propAttValue in proptAttrValues.OrderBy(propAttVal => propAttVal.attr.OrderId))
                {
                    if (propAttValue.value != null)
                    {
                        if (propAttValue.prop.PropertyType == typeof(bool))
                        {
                            propAttValue.prop.SetValue(this, true);
                        }
                        else
                        {
                            propAttValue.prop.SetValue(this, propAttValue.value);
                        }

                        sbUsages.Append(propAttValue.attr.Usage).Append(" ");
                    }
                }
            }
            catch (Exception ex)
            {

                IsValid = false;
                ErrorMessage = ex.Message;
            }
            
        }
    }
}

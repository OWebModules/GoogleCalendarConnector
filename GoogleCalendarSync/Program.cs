﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using GoogleCalendarSync.Models;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalendarSync
{
    class Program
    {
        static void Main(string[] args)
        {
            var globals = new Globals();
            var commandLine = new CommandLine(args, globals);

            if (!commandLine.IsValid)
            {
                Console.WriteLine(commandLine.ToString());
                Environment.Exit(-1);
            }

            var googleSyncConnector = new CalendarConnector(globals);

            var request = new CalendarSyncRequest(commandLine.IdGoogleSync)
            {
                DeleteGoogleEvents = commandLine.DeleteEvents
            };

            var syncResult = Task.Run<CalendarSyncResult>(() =>
            {
                return googleSyncConnector.SyncCalendar(request);
            });
            syncResult.Wait();

            if (syncResult.Result.ResultState.GUID == globals.LState_Error.GUID)
            {
                commandLine.ErrorMessage = syncResult.Result.ResultState.Additional1;
                Console.WriteLine(commandLine.ToString());
                Environment.Exit(-1);
            }
        }
    }
}

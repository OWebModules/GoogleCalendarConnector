﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GoogleCalenderConnectorTests
{
    [TestClass]
    public class CalendarConnectorTests
    {
        [TestMethod]
        public async Task TestEvents()
        {
            var connector = GlobalData.Connector;
            var request = GlobalData.GetRequest("046ff960a7174a9c88c97eb67f6734d1");


            var result = await connector.SyncCalendar(request);

            if (result.ResultState.GUID == connector.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}

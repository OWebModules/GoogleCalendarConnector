﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCalenderConnectorTests
{
    public static class GlobalData
    {
        public static CalendarConnector Connector { get; private set; } = new CalendarConnector(new OntologyAppDBConnector.Globals());

        public static CalendarSyncRequest GetRequest(string idGoogleSync)
        {
            return new CalendarSyncRequest(idGoogleSync);
        }
    }
}

﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncGoogleCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() != 1)
            {
                Console.WriteLine("Provide a Google Calendar Sync Id, please!");
                Environment.Exit(-1);
            }

            var syncId = args[0];

            var connector = new CalendarConnector(new OntologyAppDBConnector.Globals());

            if (!connector.Globals.is_GUID(syncId))
            {
                Console.WriteLine("Provide a valid Google Calendar Sync Id, please!");
                Environment.Exit(-1);
            }

            var request = new CalendarSyncRequest(syncId);

            var result =  connector.SyncCalendar(request);
            result.Wait();
            if (result.Result.ResultState.GUID == connector.Globals.LState_Error.GUID)
            {
                Console.WriteLine($"Error: {result.Result.ResultState.Additional1}");
            }
        }
    }
}
